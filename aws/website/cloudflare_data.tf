data "cloudflare_accounts" "main" {
  name = "AATF"
}

data "cloudflare_zone" "main" {
  account_id = data.cloudflare_accounts.main.accounts[0].id
  name       = "aatf.us"
}
