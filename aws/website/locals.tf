locals {
  common_tags = {
    managed_by            = "terraform"
    terraform_module_name = abspath(path.root)
  }
}
