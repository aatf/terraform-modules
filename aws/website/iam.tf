resource "aws_iam_role" "main" {
  name               = "website"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json

  tags = local.common_tags
}

resource "aws_iam_role_policy" "main" {
  name   = "s3"
  role   = aws_iam_role.main.id
  policy = data.aws_iam_policy_document.main.json
}

resource "aws_iam_instance_profile" "main" {
  name = "website"
  role = aws_iam_role.main.name
}
