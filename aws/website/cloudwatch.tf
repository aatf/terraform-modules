resource "aws_cloudwatch_log_group" "main" {
  name              = "/ecs/website"
  retention_in_days = 3

  tags = local.common_tags
}
