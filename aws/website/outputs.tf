output "aga_endpoint" {
  value = var.dev_mode ? ["no aga in dev mode"] : aws_globalaccelerator_accelerator.main[*].dns_name
}
