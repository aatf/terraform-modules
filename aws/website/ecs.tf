resource "aws_ecs_account_setting_default" "vpc_trunk" {
  name  = "awsvpcTrunking"
  value = "enabled"
}

resource "aws_ecs_cluster" "main" {
  name = "external"

  tags = local.common_tags

  setting {
    name  = "containerInsights"
    value = "enabled"
  }

  configuration {
    execute_command_configuration {
      logging = "OVERRIDE"

      log_configuration {
        cloud_watch_encryption_enabled = true
        cloud_watch_log_group_name     = aws_cloudwatch_log_group.main.name
      }
    }
  }
}

resource "aws_ecs_service" "main" {
  name                 = "website"
  cluster              = aws_ecs_cluster.main.id
  task_definition      = aws_ecs_task_definition.main.arn
  desired_count        = 2
  force_new_deployment = true
  launch_type          = "FARGATE"

  load_balancer {
    target_group_arn = aws_lb_target_group.main[0].arn
    container_name   = "website"
    container_port   = 80
  }

  network_configuration {
    subnets          = data.aws_subnets.private.ids
    security_groups  = [aws_security_group.main.id]
    assign_public_ip = false
  }

  triggers = {
    redeploy = timestamp()
  }

  tags = local.common_tags

  depends_on = [aws_iam_role_policy.main]
}

resource "aws_ecs_task_definition" "main" {
  family                   = "website"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 1024
  memory                   = 2048
  task_role_arn            = aws_iam_role.main.arn
  execution_role_arn       = aws_iam_role.main.arn
  network_mode             = "awsvpc"

  runtime_platform {

    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }

  container_definitions = jsonencode([
    {
      name      = "website"
      image     = "${var.image}"
      cpu       = 1024
      memory    = 2048
      essential = true

      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        },
      ]

      logConfiguration = {
        logDriver = "awslogs"

        options = {
          "awslogs-create-group"  = "true"
          "awslogs-group"         = "${aws_cloudwatch_log_group.main.name}"
          "awslogs-region"        = "us-west-2"
          "awslogs-stream-prefix" = "container"
        }
      }
    }
  ])

  tags = local.common_tags
}
