resource "aws_db_instance" "main" {
  count = var.dev_mode ? 0 : 1

  identifier                   = "website"
  engine                       = "mysql"
  engine_version               = "8.0.31"
  instance_class               = "db.t4g.micro"
  allocated_storage            = 20
  iops                         = 3000
  max_allocated_storage        = 1000
  vpc_security_group_ids       = [aws_security_group.main.id]
  monitoring_interval          = 60
  performance_insights_enabled = false
  skip_final_snapshot          = false
  final_snapshot_identifier    = "website-final"
  copy_tags_to_snapshot        = true
  storage_encrypted            = true
  deletion_protection          = true
  apply_immediately            = true
}
