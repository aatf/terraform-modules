resource "aws_globalaccelerator_accelerator" "main" {
  count = var.dev_mode ? 0 : 1

  name            = "website"
  ip_address_type = "DUAL_STACK"
  enabled         = true

  attributes {
    flow_logs_enabled   = true
    flow_logs_s3_bucket = aws_s3_bucket.main.id
    flow_logs_s3_prefix = "flow-logs/"
  }

  tags = local.common_tags
}

resource "aws_globalaccelerator_listener" "main" {
  count = var.dev_mode ? 0 : 1

  accelerator_arn = aws_globalaccelerator_accelerator.main[0].id
  client_affinity = "SOURCE_IP"
  protocol        = "TCP"

  port_range {
    from_port = 80
    to_port   = 80
  }

  port_range {
    from_port = 443
    to_port   = 443
  }
}

resource "aws_globalaccelerator_endpoint_group" "main" {
  count = var.dev_mode ? 0 : 1

  listener_arn = aws_globalaccelerator_listener.main[0].id

  endpoint_configuration {
    endpoint_id                    = aws_lb.main[0].arn
    weight                         = 100
    client_ip_preservation_enabled = true
  }
}
