variable "image" {
  type        = string
  description = "ecs task definition docker image"
}

variable "cdn_bucket_name" {
  type        = string
  description = "cdn bucket name"
}

variable "cdn_sans" {
  type        = list(string)
  description = "cdn subject alternative names"
  default     = []
}

variable "cdn_domain_name" {
  type        = string
  description = "cdn domain name"
}

variable "bootstrap_bucket" {
  type        = string
  description = "bootstrap bucket name"
}

variable "dev_mode" {
  type        = bool
  description = "dev mode"
  default     = false
}

variable "acm_domain_name" {
  type        = string
  description = "acm domain name"
}

variable "acm_sans" {
  type        = list(string)
  description = "acm subject alternative names"
}
