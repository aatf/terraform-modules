data "aws_ami" "amzn2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["al2023-ami-*"]
  }
}

data "aws_vpc" "vpc" {}

data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }

  filter {
    name   = "tag:type"
    values = ["private"]
  }
}

data "aws_iam_policy_document" "bucket" {
  statement {
    principals {
      type = "AWS"
      identifiers = [
        "797873946194",
      ]
    }

    actions = [
      "s3:PutObject",
    ]

    resources = [
      aws_s3_bucket.main.arn,
      "${aws_s3_bucket.main.arn}/*",
    ]
  }
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com",
        "ecs.amazonaws.com",
        "ecs-tasks.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "main" {
  statement {
    actions = [
      "s3:Get*",
      "ssmmessage:*",
      "logs:CreateLogStream",
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "logs:PutLogEvents",
    ]

    resources = [
      aws_cloudwatch_log_group.main.arn,
      "${aws_cloudwatch_log_group.main.arn}:*",
      data.aws_s3_bucket.bootstrap.arn,
      "${data.aws_s3_bucket.bootstrap.arn}/*",
    ]
  }
}

data "aws_s3_bucket" "bootstrap" {
  bucket = var.bootstrap_bucket
}

data "aws_security_group" "nat" {
  count = var.dev_mode ? 0 : 1

  name = "nat gateway"
}

data "aws_security_group" "aga" {
  count = var.dev_mode ? 0 : 1

  name = "GlobalAccelerator"
}

data "aws_secretsmanager_secret" "website" {
  name = "website"
}

data "aws_secretsmanager_secret_version" "website" {
  secret_id = data.aws_secretsmanager_secret.website.id
}

