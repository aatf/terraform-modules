resource "aws_security_group" "main" {
  name        = "website"
  description = "website service"
  vpc_id      = data.aws_vpc.vpc.id

  ingress {
    description = "allow http from alb"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.vpc.cidr_block]
  }

  ingress {
    description = "allow https from alb"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.vpc.cidr_block]
  }

  ingress {
    description = "allow all from self"
    from_port   = 0
    to_port     = 0
    protocol    = -1
    self        = true
  }

  dynamic "ingress" {
    for_each = var.dev_mode ? [] : [0]

    content {
      description     = "allow all from nat"
      from_port       = 0
      to_port         = 0
      protocol        = -1
      security_groups = [data.aws_security_group.nat[0].id]
    }
  }

  dynamic "ingress" {
    for_each = var.dev_mode ? [] : [0]

    content {
      description     = "allow all from global accelerator"
      from_port       = 0
      to_port         = 0
      protocol        = -1
      security_groups = [data.aws_security_group.aga[0].id]
    }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(local.common_tags,
    {
      Name = "website"
    }
  )
}

resource "aws_s3_bucket" "main" {
  bucket = "aatf-site"
}

resource "aws_s3_bucket_policy" "main" {
  bucket = aws_s3_bucket.main.id
  policy = data.aws_iam_policy_document.bucket.json
}

resource "aws_s3_bucket_public_access_block" "main" {
  bucket                  = aws_s3_bucket.main.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
