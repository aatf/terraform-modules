resource "aws_acm_certificate" "cdn" {
  provider = aws.east

  domain_name               = var.cdn_domain_name
  subject_alternative_names = var.cdn_sans
  validation_method         = "DNS"

  tags = local.common_tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "cdn" {
  provider = aws.east

  certificate_arn         = aws_acm_certificate.cdn.arn
  validation_record_fqdns = [for r in cloudflare_record.cdn_validation : r.hostname]
}

resource "cloudflare_record" "cdn_validation" {
  for_each = { for k, v in aws_acm_certificate.cdn.domain_validation_options : k.domain_name => v }

  zone_id = data.cloudflare_zone.main.id
  name    = each.value.resource_record_name
  type    = each.value.resource_record_type
  value   = each.value.resource_record_value
  ttl     = 1
  proxied = false
}

resource "aws_s3_bucket" "cdn" {
  bucket = var.cdn_bucket_name

  tags = local.common_tags
}

resource "aws_s3_bucket_acl" "cdn" {
  bucket = aws_s3_bucket.cdn.id
  acl    = "public-read"
}

resource "cloudflare_record" "cdn" {
  zone_id = data.cloudflare_zone.main.id
  name    = var.cdn_domain_name
  type    = "CNAME"
  value   = aws_cloudfront_distribution.cdn.domain_name
}

resource "aws_cloudfront_distribution" "cdn" {
  enabled         = true
  is_ipv6_enabled = true
  aliases         = [var.cdn_domain_name]
  http_version    = "http2and3"
  comment         = "aatf.us website cdn"

  origin {
    domain_name = aws_s3_bucket.cdn.bucket_regional_domain_name
    origin_id   = aws_s3_bucket.cdn.id
  }

  default_cache_behavior {
    allowed_methods        = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT", ]
    cached_methods         = ["GET", "HEAD", ]
    viewer_protocol_policy = "redirect-to-https"
    target_origin_id       = aws_s3_bucket.cdn.id
    compress               = true

    forwarded_values {
      query_string = true

      cookies {
        forward = "all"
      }
    }
  }

  logging_config {
    bucket          = aws_s3_bucket.main.bucket_domain_name
    include_cookies = false
    prefix          = aws_s3_bucket.cdn.id
  }

  restrictions {
    geo_restriction {

      locations        = []
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.cdn.arn
    minimum_protocol_version = "TLSv1.2_2021"
    ssl_support_method       = "sni-only"
  }

  tags = local.common_tags
}
