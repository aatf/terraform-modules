resource "aws_lb" "main" {
  count = var.dev_mode ? 0 : 1

  name               = "website"
  internal           = true
  load_balancer_type = "application"
  security_groups    = [aws_security_group.main.id]
  subnets            = data.aws_subnets.private.ids
  ip_address_type    = "dualstack"

  access_logs {
    bucket  = aws_s3_bucket.main.bucket
    prefix  = "alb"
    enabled = true
  }

  tags = merge(local.common_tags,
    {
      Environment = "production"
    }
  )
}

resource "aws_lb_target_group" "main" {
  count = var.dev_mode ? 0 : 1

  name        = "website"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.vpc.id
  target_type = "ip"

  health_check {
    enabled  = true
    port     = 80
    path     = "/"
    protocol = "HTTP"
    matcher  = "200-399"
  }
}

resource "aws_lb_listener" "http" {
  count = var.dev_mode ? 0 : 1

  load_balancer_arn = aws_lb.main[0].arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "https" {
  count = var.dev_mode ? 0 : 1

  load_balancer_arn = aws_lb.main[0].arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-FS-1-2-Res-2020-10"
  certificate_arn   = aws_acm_certificate.main.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.main[0].arn
  }
}

resource "aws_acm_certificate" "main" {
  domain_name               = var.acm_domain_name
  subject_alternative_names = var.acm_sans
  validation_method         = "DNS"

  tags = local.common_tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "main" {
  certificate_arn         = aws_acm_certificate.main.arn
  validation_record_fqdns = [for r in cloudflare_record.main : r.hostname]
}

resource "cloudflare_record" "main" {
  for_each = { for k, v in aws_acm_certificate.main.domain_validation_options : k.domain_name => v }

  zone_id = data.cloudflare_zone.main.id
  name    = each.value.resource_record_name
  type    = each.value.resource_record_type
  value   = each.value.resource_record_value
  ttl     = 1
  proxied = false
}
